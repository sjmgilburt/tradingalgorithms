# Trading Algorithms

This personal project is an exploration of financial data and a first foray into quant trading analysis and algorithm 
development.

This project has been developed by Samuel J. M. Gilburt under the MIT License.

## Getting Started

The current version of this repository is not currently considered stable for use. Cloning this repo will provide access 
to exploratory notebooks containing data analysis techniques and simple trading algorithms built for exploratory and 
educational purposes only.

Below are the prerequisites to run the Jupyter notebooks.

### Prerequisites

* [Git](https://git-scm.com/)
* [Python 3.8](https://www.python.org/downloads)
* [Matplotlib](https://matplotlib.org/)
* [NumPy](https://numpy.org/)
* [pandas](https://pandas.pydata.org/)
* [Quandl](https://www.quandl.com/)
* [statsmodels](https://www.statsmodels.org/)

## Deployment

The current version is a development version only and should not be considered stable for wider deployment at this time. 

## Versioning

This project uses [SemVer](http://semver.org/) for versioning. For the versions available, see 
[Releases](https://gitlab.com/sjmgilburt/tradingalgorithms/-/releases).

## Authors

See the list of [contributors](https://gitlab.com/gilburtgtr/tradingalgorithms/graphs/master) who participated in 
this project.

## Licence

This project is licensed under the MIT License - see the [LICENCE.md](LICENCE.md) file for details.

## Acknowledgements

* [roughen.glyph](https://thenounproject.com/roughen.glyph/) - project icon
* [Karlijn Willems](https://www.datacamp.com/profile/karlijn) - Python algorithmic trading tutorial
